import React from 'react';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import Popover, { PopoverAnimationVertical } from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import Avatar from 'material-ui/Avatar';
import styles from './Header.scss';
import * as actions from '../../../../services/auth/actions';

class Header extends React.Component {
    state = { open: false };
    logout = () => this.props.deauthenticate(this.props.history);
    handleClick = (event) => {
        this.setState({
            open: true,
            anchorEl: event.currentTarget
        });
    };

    handleRequestClose = () => {
        this.setState({
            open: false
        });
    };
    render() {
        return (
            <div className={styles.header}>
                <div className={styles['nav-left']}>
                    <IconButton
                        iconStyle={{ color: '#72777a' }}
                        onClick={this.props.changeCollapse}
                    >
                        <MenuIcon />
                    </IconButton>
                </div>
                <div className={styles['nav-right']} onClick={this.handleClick}>
                    <Avatar size={30}>A</Avatar>
                    <span>Admin</span>
                    <Popover
                        open={this.state.open}
                        anchorEl={this.state.anchorEl}
                        anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                        targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                        onRequestClose={this.handleRequestClose}
                        animation={PopoverAnimationVertical}
                    >
                        <Menu>
                            <MenuItem primaryText="Log out" onClick={this.logout} />
                        </Menu>
                    </Popover>
                </div>
            </div>
        );
    }
}

export default connect(null, actions)(Header);
