import React from 'react';
import styles from './Content.scss';

const Content = ({ children }) => {
    return (
        <div className={styles.content}>
            <div className="container">
                {children}
            </div>
        </div>
    );
};

export default Content;
