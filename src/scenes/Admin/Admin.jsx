import React from 'react';
import { Route, Switch } from 'react-router-dom';
import styles from './Admin.scss';

import requireAuth from '../../common/RequireAuth/RequireAuth';
import Notifications from '../../common/Notifications/Notifications';

import Sidebar from './components/LeftBar/LeftBar';
import Header from './components/Header/Header';
import Content from './components/Content/Content';

import NewPost from './scenes/NewPost/NewPost';
import PostsTable from './scenes/PostsTable/PostsTable';

class Admin extends React.Component {
    state = {
        collapsed: false
    };
    changeCollapse = (e) => {
        this.setState({ collapsed: !this.state.collapsed });
        e.stopPropagation();
    };
    closeSidebar = (e) => {
        this.setState({ collapsed: false });
    };
    render() {
        const { collapsed } = this.state;
        const style = collapsed
            ? 'is-collapsed'
            : 'page-container';
        return (
            <div>
                <Sidebar collapsed={collapsed} closeSidebar={this.closeSidebar} />
                <div className={styles[style]} onClick={this.closeSidebar}>
                    <Header
                        changeCollapse={this.changeCollapse}
                        history={this.props.history}
                    />
                    <Content>
                        <Switch>
                            <Route
                                path={`${this.props.match.url}/posts/new`}
                                component={NewPost}
                            />
                            <Route path="/" component={PostsTable} />
                        </Switch>
                        {/* <Route path="/posts/:id/edit" /> */}
                    </Content>
                </div>
            </div>
        );
    }
}

export default requireAuth(Notifications(Admin));
