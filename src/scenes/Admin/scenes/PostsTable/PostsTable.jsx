import map from 'lodash/map';
import React from 'react';
import { connect } from 'react-redux';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow
} from 'material-ui/Table';
import PostsTableRow from './components/PostsTableRow/PostsTableRow';
import Pagination from '../../../../common/Pagination/Pagination';
import * as actions from '../../../../services/posts/actions';

class PostsTable extends React.Component {
    deletePost = (id) => {
        this.props.deletePost(id, () => {
            this.props.update();
        });
    }
    btn = { width: '72px' };
    render() {
        return (
            <div style={{ overflowX: 'auto', marginBottom: 25 }}>
                <Table
                    wrapperStyle={{ minWidth: '700px' }}
                    selectable={false}
                >
                    <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                    >
                        <TableRow>
                            <TableHeaderColumn>Title</TableHeaderColumn>
                            <TableHeaderColumn>Description</TableHeaderColumn>
                            <TableHeaderColumn style={{ width: '150px' }}>
                                Created at
                            </TableHeaderColumn>
                            <TableHeaderColumn style={this.btn} />
                            <TableHeaderColumn style={this.btn} />
                        </TableRow>
                    </TableHeader>
                    <TableBody showRowHover={true} displayRowCheckbox={false}>
                        {map(this.props.data, post => {
                            return <PostsTableRow
                                key={post._id}
                                post={post}
                                deletePost={this.deletePost}
                            />})}
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default Pagination({
    perPage: 6,
    loadDataAction: actions.fetchPosts,
    appStateName: 'posts'
})(connect(null, actions)(PostsTable));
