import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import styles from './Login.scss';
import * as actions from '../../../services/auth/actions';
import { required, email } from '../../../utils/validation';
import LoadingIcon from './components/LoadingIcon/LoadingIcon';

class SignIn extends React.Component {
    defaultState = {
        lineHeight: '36px',
        labelStyle: {},
        sending: false
    };
    state = { ...this.defaultState };
    componentWillMount() {
        if (this.props.authenticated) {
            this.props.history.push('/admin');
        }
    }
    componentWillUnmount() {
        this.props.authError(null);
    }
    onFormSubmit = (values) => {
        this.setState({
            lineHeight: '32px',
            labelStyle: {
                display: 'none'
            },
            sending: true
        });
        this.props.authenticate('signin', values, this.props.history, () => {
            this.setState({ ...this.defaultState });
        });
    }
    fields = [
        {
            name: 'email',
            options: {
                type: 'text',
                placeholder: 'Enter email',
                autoComplete: 'off'
            },
            validate: [required, email]
        },
        {
            name: 'password',
            options: {
                type: 'password',
                placeholder: 'password',
                autoComplete: 'off'
            },
            validate: [required]
        }
    ];
    renderInput({ input, options, meta }) {
        const { touched, error } = meta;
        return (
            <TextField
                style={{ margin: '10px 0' }}
                errorText={touched ? error : ''}
                {...options}
                {...input}
            />
        );
    }
    renderField = (options, index) => {
        return (
            <Field
                key={index}
                {...options}
                component={this.renderInput}
            />
        );
    }
    render() {
        const { handleSubmit, error } = this.props;
        return (
            <section className={['container-fluid', styles.signin].join(' ')}>
                <div className="row">
                    <div className={['col-lg-8 col-md-6', styles.left].join(' ')} />
                    <div className={['col-lg-4 col-md-6', styles.right].join(' ')}>
                        <h1>Login</h1>
                        {error && <div className={styles.error}>{error}</div>}
                        <form onSubmit={handleSubmit(this.onFormSubmit)}>
                            {this.fields.map(this.renderField)}
                            <div>
                                <RaisedButton
                                    label="login"
                                    type="submit"
                                    primary={true}
                                    disabled={this.state.sending}
                                    style={{ margin: 12, verticalAlign: true }}
                                    overlayStyle={{ lineHeight: this.state.lineHeight }}
                                    labelStyle={this.state.labelStyle}
                                >
                                    {this.state.sending
                                        ? <LoadingIcon style={{ verticalAlign: 'middle' }} />
                                        : ''}
                                </RaisedButton>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        );
    }
}

const mapState = ({ auth: { authenticated, error } }) => {
    return { authenticated, error };
};

export default reduxForm({
    form: 'SignIn'
})(connect(mapState, actions)(SignIn));
