import React from 'react';
import Banner from './components/Banner/Banner';
import ArticlesList from './components/ArticlesList/ArticlesList';

import '../../../../styles/Fonts/style.scss';

const Home = () => {
    return (
        <div>
            <Banner />
            <ArticlesList />
        </div>
    );
};

export default Home;
