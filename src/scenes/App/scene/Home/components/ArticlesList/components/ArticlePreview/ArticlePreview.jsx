import React from 'react';
import { Link } from 'react-router-dom';
import {
    Card,
    CardMedia,
    CardTitle,
    CardText
} from 'material-ui/Card';
import moment from 'moment';
import './ArticlePreview.scss';

const ArticlePreview = ({
    _id,
    description,
    createdAt,
    imagePreview,
    title
}) => {
    const path = `/posts/${_id}`;
    return (
        <div
            key={_id}
            className="col-lg-4 col-md-6 col-sm-8"
            style={{
                padding: '0 30px',
                marginBottom: '20px'
            }}
        >
            <Card style={{ height: '485px', overflow: 'hidden' }}>
                <Link to={path}>
                    <CardMedia
                        style={{
                            height: '280px',
                            backgroundSize: 'cover',
                            backgroundOrigin: 'border-box',
                            backgroundPosition: '50% 50%',
                            backgroundImage: `url(${API_URL}/images/posts/${imagePreview})`
                        }}
                    />
                </Link>
                <Link to={path}>
                    <CardTitle
                        title={title}
                        titleStyle={{
                            lineHeight: '24px',
                            fontSize: '21px'
                        }}
                    />
                </Link>
                <Link to={path}>
                    <CardText
                        color="rgba(0,0,0,.54)"
                        style={{ paddingTop: 0 }}
                    >
                        {description}
                    </CardText>
                </Link>
            </Card>
        </div>
    );
};

export default ArticlePreview;
