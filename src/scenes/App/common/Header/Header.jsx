import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import IconButton from 'material-ui/IconButton';
import Media from 'react-media';
import styles from './Header.scss';

class Header extends React.Component {
    // state = { active: false };
    // showMenu = () => {
    //     this.setState({
    //         active: !this.state.active
    //     });
    // }

    render() {
        // const list = (
        //     <NavLink
        //         to="/"
        //         className="nav-item nav-link"
        //         activeClassName={styles.active}
        //     >
        //         Home
        //     </NavLink>
        // );
        return (
            <div className={['container-fluid', styles.header].join(' ')}>
                <div className="container">
                    <nav className="navbar navbar-expand-lg" style={{ padding: 0 }}>
                        <Link to="/" className="navbar-brand">
                            <div className={styles.brand}>
                                <img
                                    className={styles.logo}
                                    src={require('../../../../../img/logo.png')}
                                />
                                <h1 className={styles['logo-name']}>Bitcoin Blog</h1>
                            </div>
                        </Link>
                        {/*<div className={['navbar-nav', styles.nav].join(' ')}>*/}
                            {/*<Media query="(min-width: 768px)">*/}
                                {/*{matches =>*/}
                                    {/*matches ?*/}
                                        {/*list*/}
                                        {/*: (*/}
                                            {/*<IconButton*/}
                                                {/*onClick={this.showMenu}*/}
                                                {/*iconClassName="fas fa-bars"*/}
                                            {/*/>*/}
                                        {/*)*/}
                                {/*}*/}
                            {/*</Media>*/}
                        {/*</div>*/}
                    </nav>
                </div>
                {/*<Media*/}
                    {/*query="(max-width: 768px)"*/}
                    {/*render={() =>*/}
                        {/*<div className={[styles.Qwerty, ((this.state.active) ? styles.show : '')].join(' ')}>*/}
                            {/*{list}*/}
                        {/*</div>*/}
                    {/*}*/}
                {/*/>*/}
            </div>
        );
    }
}

export default Header;

{/*<li><a href=" "> bcc exchange </a></li>*/}
{/*<li><a href=" "> bcc lending </a></li>*/}
{/*<li><a href=" "> bitcoin guide </a></li>*/}
{/*<li><a href=" "> fags </a></li>*/}
{/*<li><NavLink to=" "> bitcoin news </NavLink></li>*/}
{/*<a href=" " > <img src={require('../../../../img/search.png')} alt="" />  </a>*/}
