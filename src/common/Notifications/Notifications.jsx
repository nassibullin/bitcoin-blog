import React from 'react';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import * as actions from '../../services/notifications/actions';

export default function(Component) {
    class Notifications extends React.Component {
        componentDidMount() {
            this.notify();
        }
        componentDidUpdate(prevProps) {
            this.notify();
        }
        componentWillUnmout() {
            this.props.newNotification(null);
        }
        notify = () => {
            const { notifications } = this.props;
            if (notifications) {
                toast.info(notifications, {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
                this.props.newNotification(null);
            }
        }
        render() {
            return (
                <div>
                    <Component {...this.props} />
                    <ToastContainer />
                </div>
            );
        }
    }
    const mapState = ({ notifications }) => {
        return { notifications };
    };
    return connect(mapState, actions)(Notifications);
}
