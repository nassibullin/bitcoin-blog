import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export default function (Component) {
    class RequireAuth extends React.Component {
        static contextTypes = {
            router: PropTypes.object
        }
        componentWillMount() {
            if (!this.props.authenticated) {
                this.context.router.history.push('/signin');
            }
        }
        render() {
            return (
                <Component {...this.props} />
            );
        }
    }
    const mapState = ({ auth: { authenticated } }) => {
        return { authenticated };
    };
    return connect(mapState)(RequireAuth);
}
