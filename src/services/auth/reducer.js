import {
    AUTH_USER,
    DEAUTH_USER,
    AUTH_ERROR
} from '../types';

const initialState = {
    authenticated: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case AUTH_USER:
            return { ...state, authenticated: true };
        case DEAUTH_USER:
            return { ...state, authenticated: false };
        case AUTH_ERROR:
            return { ...state, error: action.payload };
        default:
            return state;
    }
}
