import { NEW_NOTIFICATION } from '../types';

export function newNotification(message) {
    return {
        type: NEW_NOTIFICATION,
        payload: message
    };
}
