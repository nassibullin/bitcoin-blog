import axios from 'axios';
import _ from 'lodash';
import { newNotification } from '../notifications/actions';
import {
    NEW_POST,
    FETCH_POSTS,
    FETCH_POST,
    DELETE_POST
} from '../types';

export function newPost(data, history, cb) {
    return dispatch => {
        let formData = new FormData();
        _.forEach(data, (key, value) => formData.append(value, key));
        axios.post(`${API_URL}/posts`, formData, {
            headers: { 'content-type': 'multipart/form-data' }
        })
            .then(res => {
                dispatch({ type: NEW_POST, payload: res.data.post });
                dispatch(newNotification('Post has been successfully created'));
                cb();
                history.push('/admin');
            })
            .catch(err => console.log(err));
    };
}

export function fetchPosts(query = '', cb) {
    return dispatch => {
        axios.get(`${API_URL}/posts?${query}`)
            .then(res => {
                dispatch({ type: FETCH_POSTS, payload: res.data });
                if (typeof cb === 'function') { cb(res.data.posts); }
            })
            .catch(err => console.log(err));
    };
}

export function fetchPost(id) {
    return dispatch => {
        axios.get(`${API_URL}/posts/${id}`)
            .then(res => dispatch({ type: FETCH_POST, payload: res.data.post }))
            .catch(err => console.log(err));
    };
}

export function deletePost(id, cb) {
    return dispatch => {
        axios.delete(`${API_URL}/posts/${id}`)
            .then(() => {
                dispatch({ type: DELETE_POST, payload: id });
                dispatch(newNotification('Post has been successfully deleted'));
                if (typeof cb === 'function') { cb(); }
            })
            .catch(err => console.log(err));
    };
}
